#!/bin/bash

# Usage
# log in to aws
#
# source this file
# 'source ./aws_log_search.sh'

# gets the current time in milliseconds
current_millis(){
  date +%s%N | cut -b1-13
}

# list log groups
# param1 = dev | stage | dev${featurebranch} => devpde-396
# param2 prefix search string 'identity-service'
# exp: 'list_log_groups dev identity'
# 'list_log_groups devpde-770 provider'
list_log_groups(){
  aws logs describe-log-groups --log-group-name-prefix /aws/lambda/"$1_$2" | jq ".logGroups[] | .logGroupName"
}

# search a log group with a start time
# param1 log group
# param2 filter string
# param3 epoch millisecond time to start search
# exp: search_log /aws/lambda/stage_identity-service_usersPost "{ $.message = \"Timer results for *\" && $.data.duration > 5000 }" 1536234064326
search_log(){
  aws logs filter-log-events --log-group-name "$1" --filter-pattern "$2" --start-time "$3" | jq ".events[] | .message" | jq -r "." | cut -f3- -d$'\t' | jq -s '.'
}
